# Formación BB2

_Api Rest React, SPI pequeño software para la gestión de mercancías en una empresa._

## Comenzando 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local._
 * [Clonar](https://gitlab.com/Daniel.sanluis/react-hooks-redux-formacion-bb2.git)


## Construido con 🛠️

_Stack Tecnológico y librerías_

* [NODE](https://nodejs.org/es/) - Entorno en tiempo de ejecución multiplataforma, de código abierto, para la capa del servidor basado en el lenguaje de programación JavaScript.
* [REACT](https://es.reactjs.org/) - Una biblioteca de JavaScript para construir interfaces de usuario.
* [REDUX](https://es.redux.js.org/) - Redux es un contenedor predecible del estado de aplicaciones JavaScript.
* [THUNK](https://github.com/reduxjs/redux-thunk) - Middleware recomendado para la lógica básica de efectos secundarios de Redux, incluida la lógica síncrona compleja que necesita acceso a la tienda y la lógica asíncrona simple como las solicitudes AJAX.
* [SWEETALERT](https://sweetalert.js.org/guides/) - Libería de alertas



## Autores ✒️

* **Daniel San Luis Acosta** 

