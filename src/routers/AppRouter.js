import React from 'react'
import { useSelector } from 'react-redux';
import {
    BrowserRouter as Router,
    Switch,
    Route
  } from 'react-router-dom';
import { LoginScreen } from '../components/auth/LoginScreen';
import { DashboardRoutes } from './DashboardRoutes';
import { PrivateRoute } from './PrivateRoute';


export const AppRouter = () => {

    const userState = useSelector(state => state.auth);

    console.table("Approuter ---> ", userState)

    return (
        <Router>
            <div>
                <Switch> 
                    <Route exact path="/login" component={ LoginScreen } />
                                       
                    <PrivateRoute 
                        path="/" 
                        component={ DashboardRoutes } 
                        isAuthenticated={userState && userState.logged}
                    />
                </Switch>
            </div>
        </Router>
    )
}
