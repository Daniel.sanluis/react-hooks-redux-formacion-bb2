import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import { ItemTable } from "../components/item/ItemTable";
import { Footer } from "../components/ui/Footer";
import { Navbar } from "../components/ui/NavBar";
import { ItemDetails } from "../pages/ItemDetails";
import { Itemscreen } from "../pages/ItemScreen";
import { UserScreen } from "../pages/UserScreen";
import { useDispatch } from 'react-redux';
import { PublicRoute } from './PublicRoute';
import { PrivateRoute } from './PrivateRoute';

export const DashboardRoutes = () => {

    

  return (
    <>
      <Navbar />

      <div className="container mt-2">
        <Switch>
          <Route exact path="/item" component={Itemscreen} />
          <Route exact path="/user" component={UserScreen} />

          <Route exact path="/itemDetails/:id" component={ItemDetails} />
          <Redirect to="/login" />
        </Switch>
      </div>

      {/* <Footer /> */}
    </>
  );
};
