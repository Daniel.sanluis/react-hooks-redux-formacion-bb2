
import { types } from "../types/types"

const baseUrl = "http://localhost:8080/login"


export const startLogin = (username, password) => {
    return (dispatch) => {
        let raw = "";

        let requestOptions = {
            method: 'POST',
            body: raw,
            redirect: 'follow'
        };

        fetch(`${baseUrl}?user=${username}&password=${password}`, requestOptions )
            .then(response => response.text())
            .then(result => {
                const user = JSON.parse(result);
                dispatch(login(user.idUser, user.username, user.token, user.roles));
            })




    };

}


export const login = (idUser, username, token, roles) => {
    return {
        type: types.login,
        payload: {
            idUser,
            username,
            token,
            roles
        }
    }
}


