import { getSuppliers } from "../services/supplier/getSuppliers";
import { types } from "../types/types";

export const loadSuppliers = () => {
  return async (dispatch) => {
    return await getSuppliers()
      .then((response) => {
        dispatch({
          type: types.supplierLoad,
          payload: response,
        });
      })
      .catch((e) => console.log(e));
  };
};
