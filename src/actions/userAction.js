import { deleteUser } from "../services/user/deleteUser";
import { getUsers } from "../services/user/getUsers";
import { saveUser } from "../services/user/saveUser";
import { updateUser } from "../services/user/updateUser";
import { types } from "../types/types";

export const loadUsers = () => {
  return async (dispatch) => {
    return await getUsers()
      .then((response) => {
        dispatch({
          type: types.userLoad,
          payload: response,
        });
      })
      .catch((e) => console.log(e));
  };
};


export const addUser = (user) => {
  return async (dispatch) => {
    return await saveUser(user).then((response) => {
      dispatch({
        type: types.userAddNew,
        payload: response,
      });
    });
  };
};

export const actualizeUser = (user) => {
  return async (dispatch) => {
    return await updateUser(user).then((response) => {
      dispatch({
        type: types.userUpdate,
        payload: user,
      });
    });
  };
};

export const removeUser = (id) => {
    return async (dispatch) => {
      return await deleteUser(id).then((response) => {
        dispatch({
          type: types.userDelete,
          payload: id,
        });
      });
    };
  };