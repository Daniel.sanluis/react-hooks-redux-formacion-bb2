
import { getRoles } from "../services/role/getRoles";
import { types } from "../types/types";

export const loadRoles = () => {
  return async (dispatch) => {
    return await getRoles()
      .then((response) => {
        dispatch({
          type: types.roleLoad,
          payload: response,
        });
      })
      .catch((e) => console.log(e));
  };
};
