import { deleteItem } from "../services/item/deleteItem";
import { getItemById } from "../services/item/getItemById";
import { getItems } from "../services/item/getItems";
import { saveItem } from "../services/item/saveItem";
import { updateItem } from "../services/item/updateItem";
import { types } from "../types/types";

export const loadItems = () => {
  return async (dispatch) => {
    return await getItems()
      .then((response) => {
        dispatch({
          type: types.itemLoad,
          payload: response,
        });
      })
      .catch((e) => console.log(e));
  };
};

export const loadItemById = (id) => {
  return async (dispatch) => {
    return await getItemById(id)
      .then((response) => {
        dispatch({
          type: types.itemLoadById,
          payload: response,
        });
      })
      .catch((e) => console.log(e));
  };
};

export const removeItem = (id) => {
  return async (dispatch) => {
    return await deleteItem(id).then((response) => {
      dispatch({
        type: types.itemDelete,
        payload: id,
      });
    });
  };
};

export const addItem = (item) => {
  return async (dispatch) => {
    return await saveItem(item).then((response) => {
      dispatch({
        type: types.itemAddNew,
        payload: response,
      });
    });
  };
};

export const actualizeItem = (item) => {
  return async (dispatch) => {
    return await updateItem(item).then((response) => {
      dispatch({
        type: types.itemUpdate,
        payload: item,
      });
    });
  };
};
