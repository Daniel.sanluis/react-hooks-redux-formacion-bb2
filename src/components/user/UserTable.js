import React from "react";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { actualizeUser, addUser, loadUsers, removeUser } from "../../actions/userAction";
import { makeStyles } from "@material-ui/core/styles";
import { Edit, Delete, Visibility } from "@material-ui/icons";
import Fab from "@material-ui/core/Fab";
import Select from "@material-ui/core/Select";
import NativeSelect from "@material-ui/core/NativeSelect";
import Switch from "@material-ui/core/Switch";
import InputLabel from "@material-ui/core/InputLabel";
import AddIcon from "@material-ui/icons/Add";
import {
  Table,
  TableContainer,
  TableHead,
  TableCell,
  TableBody,
  TableRow,
  Modal,
  Button,
  TextField,
} from "@material-ui/core";
import { loadRoles } from "../../actions/roleAction";

const useStyles = makeStyles((theme) => ({
  modal: {
    position: "absolute",
    width: 400,
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #000",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
  },

  margin: {
    margin: theme.spacing(1),
  },
  iconos: {
    cursor: "pointer",
  },
  inputMaterial: {
    width: "100%",
  },
}));

export const UserTable = ({ history }) => {
  const [modalSave, setModalSave] = useState(false);
  const [modalDelete, setModalDelete] = useState(false);
  const [modalEdit, setModalEdit] = useState(false);

  const [roleSelected, setRoleSelected] = useState({
    idRole: "",
    role: "",
    description: "",
  });

  const [userSelected, setUserSelected] = useState({
    idUser: "",
    username: "",
    password: "",
    token: null,
    enabled: true,
    roles: roleSelected,
  });

  const styles = useStyles();

  const dispatch = useDispatch();

  const users = useSelector((state) => state.users.users);

  const roles = useSelector((state) => state.roles.roles);

  useEffect(() => {
    dispatch(loadUsers());
    dispatch(loadRoles());
  }, []);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setUserSelected((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };

  const handleChangeMultiple = (event) => {
    const { options } = event.target;
    const value = [];
    for (let i = 0, l = options.length; i < l; i += 1) {
      if (options[i].selected) {
        value.push(JSON.parse(options[i].value));
      }
    }
    setUserSelected((prevState) => ({
      ...prevState,
      roles: value,
    }));
  };

  const selectUser = (user, mode) => {
    setUserSelected(user);
    mode === "Edit" ? setModalEdit(true) : modalOpenCloseDelete(true);
  };

  const checkEnable = (value) => {
    if (value) {
      return (
        <Switch        
        disabled
        checked
        color="primary"
        inputProps={{ 'aria-label': 'primary checkbox' }}
      />
      );
    } else {
      return (
        <Switch
          disabled        
          color="primary"
          inputProps={{ "aria-label": "primary checkbox" }}
        />
      );
    }
  };

  const handleSave = () => {
    dispatch(addUser(userSelected));
    modalOpenCloseSave();
  };

  const handleUpdate = () => {
    dispatch(actualizeUser(userSelected));
    modalOpenCloseEdit();
  }

  const handleDelete = () => {
    dispatch(removeUser(userSelected.idUser));
    modalOpenCloseDelete();
  };

  const modalOpenCloseSave = () => {
    setModalSave(!modalSave);
  };

  const modalOpenCloseEdit = () => {
    setModalEdit(!modalEdit);
  };

  const modalOpenCloseDelete = () => {
    setModalDelete(!modalDelete);
  };

  const bodyModalSave = (
    <form onSubmit={() => handleSave()}>
      <div className={styles.modal}>
        <h3>Add new User</h3>
        <br />
        <TextField
          name="username"
          required
          className={styles.inputMaterial}
          label="Username"
          onChange={handleChange}
        />
        <br />
        <TextField
          name="password"
          required
          className={styles.inputMaterial}
          label="Password"
          onChange={handleChange}
        />

        <InputLabel className="mt-3">Select Suppliers</InputLabel>

        <Select
          multiple
          native
          required
          name="roles"
          className={styles.inputMaterial}
          label="Roles"
          onChange={handleChangeMultiple}
          inputProps={{
            id: "select-multiple-native",
          }}
        >
          {roles &&
            roles.map((role) => (
              <option key={role.idRole} value={JSON.stringify(role)}>
                {role.role}
              </option>
            ))}
        </Select>
        <br />
        <InputLabel className="mt-3" >Enabled</InputLabel>
        <NativeSelect
          name="enabled"
          className={styles.inputMaterial}
          label="Enabled"         
          onChange={handleChange}>
          <option value= 'true'>TRUE</option>
          <option value= 'false'>FALSE</option>
        </NativeSelect>
        <br />
        <div align="right">
          <Button type="submit" color="primary">
            Save
          </Button>
          <Button onClick={modalOpenCloseSave}>Cancel</Button>
        </div>
      </div>
    </form>
  );

  const bodyModalEdit = (

    <form onSubmit={() => handleUpdate()}>

      <div className={styles.modal}>
        <h3>Update User</h3>
        <br />
        <TextField
          name="username"
          required
          className={styles.inputMaterial}
          label="Username"
          onChange={handleChange}
          value={userSelected && userSelected.username}
        />
        <br />
        <TextField
          name="password"
          required
          className={styles.inputMaterial}
          label="Password"
          onChange={handleChange}
          value={userSelected && userSelected.password}
        />
        <InputLabel className="mt-3">Select Roles</InputLabel>
        <Select
          multiple
          native
          required
          name="roles"
          className={styles.inputMaterial}
          label="Roles"
          onChange={handleChangeMultiple}
          inputProps={{
            id: "select-multiple-native",
          }}
        >
          {roles &&
            roles.map((role) => (
              <option key={role.idRole} value={JSON.stringify(role)}>
                {role.role}
              </option>
            ))}
        </Select>
        <br />
        <InputLabel className="mt-3" >Enabled</InputLabel>
        <NativeSelect
          name="enabled"
          className={styles.inputMaterial}
          label="Enabled"         
          onChange={handleChange}>
          <option value= 'true'>TRUE</option>
          <option value= 'false'>FALSE</option>
        </NativeSelect>      
        <br />
        <div align="right">
          <Button type="submit" color="primary">
            Edit
        </Button>
          <Button onClick={modalOpenCloseEdit}>Cancel</Button>
        </div>
      </div>
    </form>
  );

  const bodyModalDelete = (
    <div className={styles.modal}>
      <p>
        Are you sure you want to delete the user:{" "}
        <b>{userSelected && userSelected.username}</b> ?{" "}
      </p>
      <div align="right">
        <Button color="secondary" onClick={() => handleDelete()}>
          Yes
        </Button>
        <Button onClick={() => modalOpenCloseDelete()}>No</Button>
      </div>
    </div>
  );


  return (
    <>
      <div className="text-center">
        <br />
        <Fab color="primary" aria-label="add">
          <AddIcon
            onClick={() => modalOpenCloseSave()}
            className={styles.iconos}
          />
        </Fab>

        <br />
        <br />
        <TableContainer>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>#ID</TableCell>
                <TableCell>USERNAME</TableCell>
                <TableCell>ROLES</TableCell>
                <TableCell>PASSWORD</TableCell>
                <TableCell>ENABLED</TableCell>
                <TableCell>ACTIONS</TableCell>
              </TableRow>
            </TableHead>

            <TableBody>
              {users &&
                users.map((user) => (
                  <TableRow key={user.idUser}>
                    <TableCell>#{user.idUser}</TableCell>
                    <TableCell>{user.username}</TableCell>
                    <TableCell>
                      {user.roles &&
                        user.roles.map((role, index) => {
                          return role.role + " ";
                        })}
                    </TableCell>
                    <TableCell>{user.password}</TableCell>
                    <TableCell>
                      {checkEnable(user.enabled)}
                    </TableCell>
                    {/* <TableCell>{user.enabled.toString()}</TableCell> */}
                    <TableCell>
                      <Edit onClick={() => selectUser(user, 'Edit')} className={styles.iconos} />
                      &nbsp;&nbsp;&nbsp;
                      <Delete
                        onClick={() => selectUser(user, "Delete")}
                        color="secondary"
                        className={styles.iconos}
                      />
                    </TableCell>
                  </TableRow>
                ))}
            </TableBody>
          </Table>
        </TableContainer>

        <Modal open={modalSave} onClose={modalOpenCloseSave}>
          {bodyModalSave}
        </Modal>

        <Modal open={modalEdit} onClose={modalOpenCloseSave}>
          {bodyModalEdit}
        </Modal>

        <Modal open={modalDelete} onClose={modalOpenCloseDelete}>
          {bodyModalDelete}
        </Modal>
      </div>
    </>
  );
};
