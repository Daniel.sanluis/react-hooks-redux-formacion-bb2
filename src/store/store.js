import { createStore, combineReducers, applyMiddleware, compose } from "redux";
import { authReducer } from "../reducers/authReducer";
import { userReducer } from "../reducers/userReducer";
import {itemReducer} from '../reducers/itemReducer';
import thunk from 'redux-thunk';
import { supplierReducer } from "../reducers/supplierReducer";
import { itemByIdReducer } from "../reducers/itemByIdReducer";
import { roleReducer } from "../reducers/roleReducer";

const composeEnhancers = (typeof window !== 'undefined' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) || compose;

const reducers = combineReducers({
   auth: authReducer, 
   items: itemReducer,
   users: userReducer,
   item: itemByIdReducer,
   suppliers: supplierReducer,
   roles: roleReducer,
})

export const store = createStore(
    reducers,
    composeEnhancers(
        applyMiddleware( thunk )
    )
);