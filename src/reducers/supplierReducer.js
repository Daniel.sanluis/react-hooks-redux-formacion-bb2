
import { types } from '../types/types';

export const supplierReducer = (state = [], action) => {
    switch (action.type) {
        case types.supplierLoad:
            return {
                ...state, 
                suppliers: action.payload
            }     
        

        default:
            return state;
    }
}
