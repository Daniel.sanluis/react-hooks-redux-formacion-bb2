import { types } from "../types/types";

const state= [];

export const itemReducer = (state = [], action) => {
    switch (action.type) {
        case types.itemLoad:
            return {
                ...state, 
                items: action.payload
            }           

        case types.itemDelete:
            return {          
                ...state,
                items: state.items.filter( item => item.idItem !== action.payload )
            }

        case types.itemAddNew:     
            return{
                ...state,
                items: [...state.items, action.payload ] 
            }
            
        case types.itemUpdate:
            return{
                ...state,
                items: state.items.map(
                    item => item.idItem === action.payload.idItem
                        ? action.payload
                        : item
                )
            }

        default:
            return state;
    }

}
