import { types } from "../types/types";

export const userReducer = (state = [], action) => {
  switch (action.type) {
    case types.userLoad:
      return {
        ...state,
        users: action.payload,
      };

    case types.userAddNew:
      return {
        ...state,
        users: [...state.users, action.payload],
      };

    case types.userUpdate:
      return {
        ...state,
        users: state.users.map((user) =>
          user.idUser === action.payload.idUser ? action.payload : user
        ),
      };

    case types.userDelete:
      return {
        ...state,
        users: state.users.filter((user) => user.idUser !== action.payload),
      };

    default:
      return state;
  }
};
