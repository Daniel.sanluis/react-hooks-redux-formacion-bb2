
import { types } from '../types/types';

export const roleReducer = (state = [], action) => {
    switch (action.type) {
        case types.roleLoad:
            return {
                ...state, 
                roles: action.payload
            }     
        

        default:
            return state;
    }
}
