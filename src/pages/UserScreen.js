import React from "react";
import { UserTable } from "../components/user/UserTable";

export const UserScreen = ({ history }) => {
  return (
    <>
      <h1>UserScreen</h1>
      <UserTable history={history} />
    </>
  );
};
