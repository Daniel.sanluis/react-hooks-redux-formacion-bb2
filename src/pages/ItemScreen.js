import React from "react";
import { ItemTable } from "../components/item/ItemTable";

export const Itemscreen = ({ history }) => {
  return (
    <>
      <br />
      <h3>Item List</h3>
      <hr />
      <ItemTable history={history} />
    </>
  );
};
