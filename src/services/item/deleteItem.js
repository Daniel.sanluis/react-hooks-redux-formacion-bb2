const basicUrl ="http://localhost:8080/shop/"

export const deleteItem = (id) => {


  
    var requestOptions = {
        method: 'DELETE',
        redirect: 'follow'
      };
      
  
    return fetch(`${basicUrl}deleteItem/${id}`, requestOptions)
    .then(response => response.text())
    .then(result => console.log(result))
    .catch(error => console.log('error', error));
  }