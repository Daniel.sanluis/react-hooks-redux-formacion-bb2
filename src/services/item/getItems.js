
const basicUrl ="http://localhost:8080/shop/findItems"

export const getItems = () => {
    var raw = "";

  
    var requestOptions = {
      method: "GET",      
      redirect: "follow",
    };
    
  
    return fetch(basicUrl, requestOptions)
    .then((response) => response.json())
    .then((result) => result)
    .catch((error) => console.log("error", error));
  }