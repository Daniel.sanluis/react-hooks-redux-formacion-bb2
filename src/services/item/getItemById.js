const basicUrl ="http://localhost:8080/shop/item"

export const getItemById = (id) => {
    var raw = "";

  
    var requestOptions = {
      method: "GET",      
      redirect: "follow",
    };
    

    return fetch(`${basicUrl}/${id}`, requestOptions)
    .then((response) => response.json())
    .then((result) => result )
    .catch((error) => console.log("error", error));
  }