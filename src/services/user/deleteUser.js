const basicUrl ="http://localhost:8080/"

export const deleteUser = (id) => {


  
    var requestOptions = {
        method: 'DELETE',
        redirect: 'follow'
      };
      
  
    return fetch(`${basicUrl}deleteUser/${id}`, requestOptions)
    .then(response => response.text())
    .then(result => console.log(result))
    .catch(error => console.log('error', error));
  }