const basicUrl ="http://localhost:8080/findRoles"

export const getRoles = () => {
    var raw = "";

  
    var requestOptions = {
      method: "GET",      
      redirect: "follow",
    };
    
  
    return fetch(basicUrl, requestOptions)
    .then((response) => response.json())
    .then((result) => result)
    .catch((error) => console.log("error", error));
  }