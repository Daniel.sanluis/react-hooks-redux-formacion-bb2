

export const types = {
   
    login: '[Auth] Login',
    logout: '[Auth] Logout',

    itemLoad: '[Items] Load items',
    itemLoadById: '[Items] Load item by id',
    itemAddNew: '[Items] Add item',
    itemDelete: '[Items] Delete item',
    itemUpdate: '[Items] Update item',

    supplierLoad: '[Supliers] Load suppliers',

    userLoad: '[Users] Load users',
    userAddNew: '[Users] Add user',
    userDelete: '[Users] Delete user',
    userUpdate: '[Users] Update user',

    roleLoad: '[Roles] Load roles',
}
